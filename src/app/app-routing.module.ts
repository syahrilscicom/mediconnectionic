import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckTutorial } from './providers/check-tutorial.service';
import { CheckVerification } from './providers/check-verification.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./pages/support/support.module').then(m => m.SupportModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  }, 
  {   
 
    path: 'loginverify',
    loadChildren: () => import('./pages/loginverify/loginverify.module').then(m => m.LoginVerifyModule),
    canLoad: [CheckVerification]

  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignUpModule)
  },
  {
    path: 'app',
    loadChildren: () => import('./pages/tabs-page/tabs-page.module').then(m => m.TabsModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./pages/tutorial/tutorial.module').then(m => m.TutorialModule),
    canLoad: [CheckTutorial]
  },
  {
    path: 'clinicvisit',
    loadChildren: () => import('./pages/clinicvisit/clinicvisit.module').then(m => m.ClinicVisitModule),

    
  },
  {
    path: 'mycard',
    loadChildren: () => import('./pages/mycard/mycard.module').then(m => m.MyCardModule),
  
  },
  {
    path: 'myplan',
    loadChildren: () => import('./pages/myplan/myplan.module').then(m => m.MyPlanModule),
      
  },
  {
    path: 'claims',
    loadChildren: () => import('./pages/claims/claims.module').then(m => m.ClaimsModule),
  
  },
  {
    path: 'terms',
    loadChildren: () => import('./pages/terms/terms.module').then(m => m.TermsModule),
  
  },
  {
    path: 'captureselfie',
    loadChildren: () => import('./pages/captureselfie/captureselfie.module').then(m => m.CaptureSelfieModule),
  
  },
  { path: 'second/:price', loadChildren: './pages/second/second.module#SecondPageModule' 
},

  {
    path: 'termscondition',
    loadChildren: () => import('./pages/termscondition/termscondition.module').then( m => m.TermsconditionPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
