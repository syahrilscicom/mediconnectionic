import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { MyPlanPage } from './myplan';
//import { PopoverPage } from '../about-popover/about-popover';
import { MyPlanPageRoutingModule } from './myplan-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyPlanPageRoutingModule
  ],
  declarations: [MyPlanPage],
  //entryComponents: [PopoverPage],
  bootstrap: [MyPlanPage],
})
export class MyPlanModule {}
