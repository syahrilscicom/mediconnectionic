import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MyPlanPage } from './myplan';

const routes: Routes = [
  {
    path: '',
    component: MyPlanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPlanPageRoutingModule { }
