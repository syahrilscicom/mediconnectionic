import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NotificationPage } from './notification';
import { NotificationFilterPage } from '../notification-filter/notification-filter';
import { NotificationPageRoutingModule } from './notification-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotificationPageRoutingModule
  ],
  declarations: [
    NotificationPage,
    NotificationFilterPage
  ],
  entryComponents: [
    NotificationFilterPage
  ]
})
export class NotificationModule { }
