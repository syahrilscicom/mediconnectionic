import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ClaimsPage } from './claims';
import { ClaimsFilterPage } from '../claims-filter/claims-filter';
import { ClaimsPageRoutingModule } from './claims-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClaimsPageRoutingModule
  ],
  declarations: [
    ClaimsPage,
    ClaimsFilterPage
  ],
  entryComponents: [
    ClaimsFilterPage
  ]
})
export class ClaimsModule { }
