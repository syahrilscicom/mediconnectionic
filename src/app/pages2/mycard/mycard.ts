import { Component } from '@angular/core';
import { PopoverController, MenuController } from '@ionic/angular';
import { PopoverPage } from '../about-popover/about-popover';
import { Router } from '@angular/router';
import { BarcodeScannerOptions,  BarcodeScanner} from "@ionic-native/barcode-scanner/ngx";
import { NavController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'page-myplan',
  templateUrl: 'mycard.html',
  styleUrls: ['./mycard.scss'],
})
export class MyCardPage {
  location = 'madison';
  conferenceDate = '2047-05-17';
  get_name: string = "";
  get_nric: string = "";
  get_plantype: string = "";

  card1: boolean = false;
  card2: boolean = false;
  card3: boolean = false;
  
  selectOptions = {
    header: 'Select a Location'
  };
  
  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;

  constructor(public popoverCtrl: PopoverController,private navCtrl: NavController, 
    public menuCtrl: MenuController, public router: Router, private barcodeScanner: BarcodeScanner, public storage: Storage, public screenOrientation: ScreenOrientation,private statusBar: StatusBar,
  ) {
 
     
  }

  goBack() {

    
          this.navCtrl.back();
          screen.orientation.unlock();
          screen.orientation.lock('portrait');
          // access current orientation
          console.log('Orientation is ' + screen.orientation.type);
    }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }


  ionViewWillEnter() {
    this.menuCtrl.enable(true);
this.statusBar.hide();

    var promise= this.storage.get('plantype');
    
    Promise.all([promise]).then((arrayOfResults) => {
    console.log(arrayOfResults[0]);

    this.showCard(arrayOfResults[0]);

    this.get_plantype= arrayOfResults[0];
    });
    



    var promise1= this.storage.get('name');
    
    Promise.all([promise1]).then((arrayOfResults) => {
    console.log(arrayOfResults[0]);
    this.get_name = arrayOfResults[0];
    });


    var promise2= this.storage.get('nric');
    
    Promise.all([promise2]).then((arrayOfResults) => {
    console.log(arrayOfResults[0]);
    this.get_nric= arrayOfResults[0];
    });


screen.orientation.unlock();
screen.orientation.lock('landscape');
// access current orientation
console.log('Orientation is ' + screen.orientation.type);

  }



 ionViewDidLeave()
 {
   
screen.orientation.unlock();
screen.orientation.lock('portrait');
// access current orientation
console.log('Orientation is ' + screen.orientation.type);
 }

  getCurrentOrientation()
  {
    
      console.log("Orientation 1: "+this.screenOrientation.type);
      
  }

  changeorientation()
  {

   this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);

  }

  rotate()
  {
    this.screenOrientation.unlock();

  }


detect()
{
 this.screenOrientation.onChange().subscribe(
   () => {
       console.log("Orientation Changed");
   }
);
}

  showCard(test: string)
  {

    if (test == "Employee1")
    {
        this.card1 = true;
        this.card2 = false;
        this.card3 = false;
    }
  
    if (test == "Dependant1")
    {
      this.card1 = false;
      this.card2 = true;
      this.card3 = false;
    }
  
    if (test == "Dependant2")
    {
      this.card1 = false;
      this.card2 = false;
      this.card3 = true;
    }
  }

  scanCode() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        alert("Barcode data " + JSON.stringify(barcodeData));
        this.scannedData = barcodeData;
      })
      .catch(err => {
        console.log("Error", err);
      });
  }
  /*encodedText() {
    this.barcodeScanner
      .encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeData)
      .then(
        encodeData => {
          console.log(encodeData);
          this.encodeData = encodeData;
        },
        err => {
          console.log("Error occured : " + err);
        }
      );
  }*/

  encodedText(){
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE,this.encodeData).then((encodedData) => {

        console.log(encodedData);
        this.encodeData = encodedData;

    }, (err) => {
        console.log("Error occured : " + err);
    });                 
}
}
