import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { MyCardPage } from './mycard';
//import { PopoverPage } from '../about-popover/about-popover';
import { MyCardPageRoutingModule } from './mycard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyCardPageRoutingModule
  ],
  declarations: [MyCardPage],
  //entryComponents: [PopoverPage],
  bootstrap: [MyCardPage],
})
export class MyCardModule {}
