import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MyCardPage } from './mycard';

const routes: Routes = [
  {
    path: '',
    component: MyCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyCardPageRoutingModule { }
