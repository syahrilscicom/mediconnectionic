import { Component, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { NgForm, FormControl, FormGroup, Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient} from '@angular/common/http';
import { UserData } from '../../providers/user-data';
import { LoadingController } from '@ionic/angular';
//import { Loading, LoadingController } from 'ionic-angular';
import { UserOptions } from '../../interfaces/user-options';
import {  MenuController, IonSlides } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
//import { CameraPreview, CameraPreviewPictureOptions} from '@ionic-native/camera-preview/ngx';
import { HTTP } from '@ionic-native/http/ngx';

import { Storage } from '@ionic/storage';
//import { Crop } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';
import { NavController } from '@ionic/angular';
import { Plugins } from "@capacitor/core";
const { CameraPreview } = Plugins;

@Component({
  selector: 'page-captureselfie',
  templateUrl: 'captureselfie.html',
  styleUrls: ['./captureselfie.scss'],
})

export class CaptureSelfiePage {

  
  showSkip = true;
  continue = false;
  image: string;
  base64Str: any;
  kbytes: number

  @ViewChild('slides', { static: true }) slides: IonSlides; 

  //API URL
  livenessURL: string = "https://cors-anywhere.herokuapp.com/http://18.141.100.16:8181/result";
  ocrURL: string = "https://cors-anywhere.herokuapp.com/http://18.141.100.16/result";
  matchURL: string = "https://cors-anywhere.herokuapp.com/https://iface.emgs.com.my/dev/api/matchphoto";

  combineAPI: string = "https://cors-anywhere.herokuapp.com/http://18.141.100.16:81/ekyc";

  login: UserOptions = { username: '', password: '' };
  submitted = false;

  testImage: string = "";
  public base64ImageCrop: string;
  public base64Image: string;
  public base64Image1: string;
  public mykad_image: string;
  public selfie_image: string;
  public imageSuccess: boolean = false;
  
  get_name: string = "";
  get_nric: string = "";

  signupform: FormGroup;
    userData = { "name": "", 
                 "document_no": "", 
                 "phone_no": "", 
                 "agent_name": "", 
                 "agent_phone_no": "", 
                 "image": "", 
                 "response": "", 
                 "cb": false, 
                 "cb1": false};
  
  imageURI:any;
  imageFileName:any;

  hideMe: boolean = false;
  hideMe1: boolean = false;
  livenessPass: boolean = false;
  livenessPass2: boolean = false;
  hidePreview: boolean =true;
  
  showResult: boolean = false;
  ocrPass: boolean = false;
  ocrPass2: boolean = false;

  matchPass: boolean = false;
  matchPass2: boolean = false;
  livenessloading: boolean = false;
  loadingImage: boolean = false;
  isLoading = false;

  constructor(
    //public userData: UserData,
    public router: Router,
    private camera: Camera, 
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public loadingController: LoadingController, 
    //private crop: Crop,
  //  private cameraPreview: CameraPreview,
    public storage: Storage,
    public file: File,
    private navCtrl: NavController, 
    
  ) { 

  }


  onSlideChangeStart(event) {
    event.target.isEnd().then(isEnd => {
      this.showSkip = !isEnd;
      
    });

    let me = this;
    me.slides.isEnd().then((istrue) => {
      console.log(istrue);
      if (istrue) {
        this.continue = true;
      } else {
        this.continue = false;
      }
    });

  }
  

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Click the backdrop to dismiss early...',
      translucent: true,
      cssClass: 'custom-class custom-loading',
      backdropDismiss: true
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed with role:', role);
  }

  onLogin(form: NgForm) {
 

    if (form.valid) {
     // this.userData.login(this.login.username);
    
    }
  }

  onSignup() {
    this.router.navigateByUrl('/signup');
  }
  


   ionViewWillEnter() {
    CameraPreview.start({parent: "cameraPreview2" });  
  }


  

   

  async captureSelfie()
{

  const result = await CameraPreview.capture();
  const base64PictureData = result.value;
  this.base64Image1="data:image/jpeg;base64," + base64PictureData;
  this.selfie_image =base64PictureData;
  console.log("SELFIE: "+base64PictureData);
  CameraPreview.stop();


 this.navCtrl.back();

}
  




  calculateImageSize(base64String) {
    let padding;
    let inBytes;
    let base64StringLength;
    if (base64String.endsWith('==')) { padding = 2; }
    else if (base64String.endsWith('=')) { padding = 1; }
    else { padding = 0; }

    base64StringLength = base64String.length;
    console.log(base64StringLength);
    inBytes = (base64StringLength / 4) * 3 - padding;
    console.log(inBytes);
    this.kbytes = inBytes / 1000;
    console.log("Photo size: "+this.kbytes)
    return this.kbytes;
  }


DeletePicture(test:string)
{
    if(test == "1")
    {
      this.base64Image = "";
      this.mykad_image = "";
      this.imageSuccess = false;
      this.hideMe = false;
      this.userData.cb = false; 
    }
    else
    {
      this.base64Image1 = "";
      this.selfie_image = "";
      this.imageSuccess = false;
      this.hideMe1 = false;
      this.userData.cb1 = false; 
    }
    
}

/*
getlogin()
  {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
	  const requestOptions = new RequestOptions({ headers: headers });	

	
	  var myData = JSON.stringify({email:"pdrm.officer@emgs.com.my" , password: "abc123@@"});

    this.http.post("http://mobileapp.emgs.com.my/api_v2/web/app.php/customer/login", myData, requestOptions)
      .subscribe(data => {
        console.log(data["_body"]);
       
        let a = JSON.parse(data["_body"]);

        console.log(a.isValid);

        if (a.isValid =="true"){
          console.log("User is true");
        }
        else
        {
      
        }
       

       }, error => {
        console.log(error);
      });
    }


    
getliveness()
  {

    this.showResult = true;

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });	
    
  var myData = JSON.stringify({app_id:"liveness001" , frontimage: this.selfie_image});
  
	  //var myData = JSON.stringify({email:"pdrm.officer@emgs.com.my" , password: "abc123@@"});

    console.log(myData);


    this.http.post(this.livenessURL, myData, requestOptions)
      .subscribe(data => {
        console.log(data);
        console.log(data["_body"]);
       
        let a = JSON.parse(data["_body"]);


        console.log(a.faceDetected);

        if (a.faceDetected =="1"){
          console.log("User Liveness Pass");
          this.livenessPass = true;
          this.livenessPass2 = false;

         //this.getocr();

       
        }
        else
        {
          this.livenessPass = false;
          this.livenessPass2 = true;
          console.log("Not Passed");
        }
       

       }, error => {
        this.livenessPass = false;
        this.livenessPass2 = true;
       
        console.log("Oooops!" + error);
      });


  }

  gotoNext()
  {
  
    this.router.navigateByUrl('/tutorial').then(() => this.storage.set('name', this.get_name)&&this.storage.set('ion_did_verification', true));

  }

  getmatch()
  
  {  

    this.showResult = true;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });	
    
    var myData = JSON.stringify({caller:"Mobile",  base64_source: this.selfie_image, base64_target: this.selfie_image});
  console.log(myData);

  this.http.post(this.matchURL, myData, requestOptions)
    .subscribe(data => {
      console.log(data);
      console.log(data["_body"]);
      
      let a = JSON.parse(data["_body"]);

    

      if (a.IsMatched ==true){
        console.log("Match Pass");
        this.matchPass = true;
        this.matchPass2 = false;
        //this.getocr();
      }
      else
      {
        this.matchPass = false;
        this.matchPass2 = true;
        console.log("Match Not Passed");
      }
     

     }, error => {
   
      this.matchPass = false;
      this.matchPass2 = true;
      console.log("Oooops!" + error);
    });


  }




  getresult()
  
  {  
    this.loadingImage=true;
    this.showResult = true;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });	
    
    var myData = JSON.stringify({app_id:"liveness001",  selfy: this.selfie_image, nric_data: this.mykad_image});
    console.log(myData);

  this.http.post(this.combineAPI, myData, requestOptions)
    .subscribe(data => {
      console.log(data);
      console.log(data["_body"]);
      this.loadingImage=false;
      let a = JSON.parse(data["_body"]);
      this.get_name = a.name;
      this.get_nric = a.nric;
      console.log("liveFaceDetected: "+ a.liveFaceDetected)
      console.log("name: "+ a.name)
      console.log("nric: "+ a.nric)
      console.log("ocrFaceDetected: "+ a.ocrFaceDetected)
      console.log("photoMatch: "+ a.photoMatch)
      console.log("photoMatchScore: "+ a.photoMatchScore)
      console.log("spoofConfidence: "+ a.spoofConfidence)
      if (a.liveFaceDetected ==true){
        console.log("User Liveness Pass");
        this.livenessPass = true;
        this.livenessPass2 = false;
   
      }
      else
      {
        this.livenessPass = false;
        this.livenessPass2 = true;
        console.log("Liveness Not Passed");
      }


      if (a.photoMatch ==true){
        console.log("Match Pass");
        this.matchPass = true;
        this.matchPass2 = false;
      }
      else
      {
        this.matchPass = false;
        this.matchPass2 = true;
        console.log("Match Not Passed");
      }

       if (a.ocrFaceDetected ==true){
        console.log("OCR Pass");
        this.ocrPass = true;
        this.ocrPass2 = false;
        //this.getmatch();
      }
      else
      {
        this.ocrPass = false;
        this.ocrPass2 = true;
        console.log("OCR Not Passed");
      }


     }, error => {
      this.loadingImage=false;
      console.log("Oooops!" + error);

      this.matchPass = false;
      this.matchPass2 = true;
      console.log("Match Not Passed");
      this.livenessPass = false;
      this.livenessPass2 = true;
      console.log("Liveness Not Passed");
      this.ocrPass = false;
      this.ocrPass2 = true;
      console.log("OCR Not Passed");

    });


  }
   
  getocr()
  {
    this.loadingImage=true;
    this.showResult = true;
  var headers = new Headers();
  headers.append("Accept", 'application/json');
  headers.append('Content-Type', 'application/json' );
  const requestOptions = new RequestOptions({ headers: headers });	


            var myData = JSON.stringify({
              frontimage: this.mykad_image, 
              backimagecheck: false, 
              backimage: "",
              rgbcheck: true,
              clientid: "1",
              scanid: "1",
              mykid: false
          });
  console.log(myData);

  this.http.post(this.ocrURL, myData, requestOptions)
    .subscribe(data => {
      console.log(data);
      console.log(data["_body"]);
      this.loadingImage=false;
      let a = JSON.parse(data["_body"]);
      //this.loadingImage=false;
      console.log(a.name);
      this.get_name = a.name;
      this.get_nric = a.nric ;

     

      if (a.liveFaceDetected ==true){
        console.log("OCR Pass");
        this.ocrPass = true;
        this.ocrPass2 = false;
        //this.getmatch();
      }
      else if (a.liveFaceDetected ==true){
        console.log("OCR Pass");
        this.ocrPass = true;
        this.ocrPass2 = false;
        //this.getmatch();
      }
      else if (a.liveFaceDetected ==true){
        console.log("OCR Pass");
        this.ocrPass = true;
        this.ocrPass2 = false;
        //this.getmatch();
      }
      else
      {
        this.ocrPass = false;
        this.ocrPass2 = true;
        console.log("OCR Not Passed");
      }
     

     }, error => {
      this.loadingImage=false;
      this.ocrPass = false;
      this.ocrPass2 = true;
      console.log("Oooops!" + error);
    });


  }*/
   
}
