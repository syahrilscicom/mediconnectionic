import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { CaptureSelfiePage } from './captureselfie';
import { CaptureSelfiePageRoutingModule } from './captureselfie-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CaptureSelfiePageRoutingModule
  ],
  declarations: [
    CaptureSelfiePage,
  ]
})
export class CaptureSelfieModule { }
