import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { MyMedicalPage } from './mymedical';
//import { PopoverPage } from '../about-popover/about-popover';
import { MyMedicalPageRoutingModule } from './mymedical-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyMedicalPageRoutingModule
  ],
  declarations: [MyMedicalPage],
  //entryComponents: [PopoverPage],
  bootstrap: [MyMedicalPage],
})
export class MyMedicalModule {}
