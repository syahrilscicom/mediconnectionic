import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MyMedicalPage } from './mymedical';

const routes: Routes = [
  {
    path: '',
    component: MyMedicalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyMedicalPageRoutingModule { }
