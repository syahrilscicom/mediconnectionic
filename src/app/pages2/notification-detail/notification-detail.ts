import { Component } from '@angular/core';

import { ConferenceData } from '../../providers/notification-data';
import { ActivatedRoute } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { NavController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
@Component({
  selector: 'page-notification-detail',
  styleUrls: ['./notification-detail.scss'],
  templateUrl: 'notification-detail.html'
})
export class NotificationDetailPage {
  session: any;
  isFavorite = false;
  defaultHref = '';
  speakers: any[] = [];
  constructor(
    private dataProvider: ConferenceData,private navCtrl: NavController,
    private userProvider: UserData,
    private route: ActivatedRoute,
    private statusBar: StatusBar
  ) { }

  ionViewWillEnter() {
    
    this.statusBar.backgroundColorByHexString('#2f90b0');
    this.dataProvider.load().subscribe((data: any) => {
      if (data && data.schedule && data.schedule[0] && data.schedule[0].groups) {
        const sessionId = this.route.snapshot.paramMap.get('sessionId');
        for (const group of data.schedule[0].groups) {
          if (group && group.sessions) {
            for (const session of group.sessions) {
              if (session && session.id === sessionId) {
                this.session = session;

                this.isFavorite = this.userProvider.hasFavorite(
                  this.session.name
                );

                break;
              }
            }
          }
        }
      }
    });
  }

  ionViewDidEnter() {
   // this.defaultHref = `/app/tabs/home`;

    this.dataProvider.getSpeakers().subscribe((speakers: any[]) => {
      this.speakers = speakers;
    });
  }

  goBack() {
    this.navCtrl.back();
    }
    
  sessionClick(item: string) {
    console.log('Clicked', item);
  }

  toggleFavorite() {
    if (this.userProvider.hasFavorite(this.session.name)) {
      this.userProvider.removeFavorite(this.session.name);
      this.isFavorite = false;
    } else {
      this.userProvider.addFavorite(this.session.name);
      this.isFavorite = true;
    }
  }

  shareSession() {
    console.log('Clicked share session');
  }
}
