import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs-page';
import { TabsPageRoutingModule } from './tabs-page-routing.module';
import { MyMedicalModule } from '../mymedical/mymedical.module';
import { HomeModule } from '../home/home.module';
import { MapModule } from '../map/map.module';
import { ScheduleModule } from '../schedule/schedule.module';
import { SessionDetailModule } from '../session-detail/session-detail.module';
import { NotificationDetailModule } from '../notification-detail/notification-detail.module';
import { SpeakerDetailModule } from '../speaker-detail/speaker-detail.module';
import { SpeakerListModule } from '../speaker-list/speaker-list.module';

import { NotificationModule } from '../notification/notification.module';

@NgModule({
  imports: [
    MyMedicalModule,
    HomeModule,
    CommonModule,
    IonicModule,
    MapModule,
    ScheduleModule,
    SessionDetailModule,
    NotificationDetailModule,
    SpeakerDetailModule,
    SpeakerListModule,
    TabsPageRoutingModule,
    NotificationModule
  ],
  declarations: [
    TabsPage,
  ]
})
export class TabsModule { }
