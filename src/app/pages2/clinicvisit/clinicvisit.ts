import { NavController } from '@ionic/angular';
import { Component  } from '@angular/core';
import { PopoverController, MenuController } from '@ionic/angular';
import { PopoverPage } from '../about-popover/about-popover';
import { Router } from '@angular/router';
import { BarcodeScannerOptions,  BarcodeScanner} from "@ionic-native/barcode-scanner/ngx";
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-clinicvisit',
  templateUrl: 'clinicvisit.html',
  styleUrls: ['./clinicvisit.scss'],
})
export class ClinicVisitPage {
  location = 'madison';
  conferenceDate = '2047-05-17';
  showQRResult = false;
  selectOptions = {
    header: 'Select a Location'
  };
  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;


  constructor(public popoverCtrl: PopoverController, private navCtrl: NavController,    public storage: Storage,
    public menuCtrl: MenuController, public router: Router, private barcodeScanner: BarcodeScanner
  ) {
    //this.encodeData = "ABC123";
    //Options

   
  }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }

  ionViewWillEnter() {
    //this.scanCode();

    this.scanCode();
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };

    this.menuCtrl.enable(true);
  
  }

  goBack() {
    this.navCtrl.back();
    }

    
  scanCode() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
       // alert("Barcode data " + JSON.stringify(barcodeData));
        this.scannedData = barcodeData;
        this.showQRResult=true;
      })
      .catch(err => {
        console.log("Error", err);
      });
  }
  /*encodedText() {
    this.barcodeScanner
      .encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeData)
      .then(
        encodeData => {
          console.log(encodeData);
          this.encodeData = encodeData;
        },
        err => {
          console.log("Error occured : " + err);
        }
      );
  }*/

  encodedText(){
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE,this.encodeData).then((encodedData) => {

        console.log(encodedData);
        this.encodeData = encodedData;

    }, (err) => {
        console.log("Error occured : " + err);
    });                 
}
}
