import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ClinicVisitPage } from './clinicvisit';
//import { PopoverPage } from '../about-popover/about-popover';
import { ClinicVisitPageRoutingModule } from './clinicvisit-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClinicVisitPageRoutingModule
  ],
  declarations: [ClinicVisitPage],
  //entryComponents: [PopoverPage],
  bootstrap: [ClinicVisitPage],
})
export class ClinicVisitModule {}
