import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginVerifyPage } from './loginverify';

const routes: Routes = [
  {
    path: '',
    component: LoginVerifyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginVerifyPageRoutingModule { }
