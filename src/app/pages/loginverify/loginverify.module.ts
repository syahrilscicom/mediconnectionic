import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LoginVerifyPage } from './loginverify';
import { LoginVerifyPageRoutingModule } from './loginverify-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,

    LoginVerifyPageRoutingModule
  ],
  declarations: [
    LoginVerifyPage,
  ]
})
export class LoginVerifyModule { }
