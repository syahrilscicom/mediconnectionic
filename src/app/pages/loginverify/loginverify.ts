import { Component, ViewChild } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { NgForm, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserData } from '../../providers/user-data';
import { LoadingController } from '@ionic/angular';
import { UserOptions } from '../../interfaces/user-options';
import { MenuController, IonSlides } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file/ngx';
// const { CameraPreview } = Plugins;
import { StatusBar } from '@ionic-native/status-bar/ngx';


import {
  CameraPreview,
  CameraPreviewPictureOptions,
  CameraPreviewOptions
} from '@ionic-native/camera-preview/ngx';

const SLIDES_INDEX = {
  VERIFY_SLIDE: 0,
  MYKAD_SLIDE: 1,
  CARD_PHOTO_SLIDE: 2,
  SELFIE_SLIDE: 3,  
  SELFIE_PHOTO_SLIDE: 4,
  SENDING_VERIFY_SLIDE: 5,
  TAC_SLIDE: 6,
  TAC_VERIFY_SLIDE: 7,
};

@Component({
  selector: 'page-loginverify',
  templateUrl: './loginverify.html',
  styleUrls: ['./loginverify.scss'],
})
export class LoginVerifyPage {
  // Camera // Naim
  isCameraFront = false;
  isCameraFlashMode = true;
  isCameraActive = true;

  // My Card camera options
  isMyCardCameraActive = false;
  MYCARD_SIZE = {
    width: 350,
    height: 233,
  };
  myCardCameraOption: CameraPreviewOptions = {
    x: 0,
    y: 0,
    width: window.screen.width,
    height: window.screen.height,
    camera: this.cameraPreview.CAMERA_DIRECTION.BACK,
    toBack: true,
    tapPhoto: true,
    tapFocus: true,
    previewDrag: false,
    disableExifHeaderStripping: false
  };
  myCardPictureOpts: CameraPreviewPictureOptions = {
    width: 1920,
    height: 920,
    quality: 100
  };
  myCardBase64 = '';
  mySelfieBase64 = '';

  showSkip = true;
  continue = false;
  image: string;
  base64Str: any;
  kbytes: number;

  @ViewChild('slides', { static: true }) slides: IonSlides;

  // API URL

  combineAPI: string = "http://bioapi.scicom.com.my/ekyc";  
  verifyCCS: string = "http://dev.ccs.sc/api/user/verify";
  otp:string = "https://cors-anywhere.herokuapp.com/http://18.141.110.210/otp?mobile_number=";
 // otpConfirm:string = "https://cors-anywhere.herokuapp.com/http://18.141.110.210/verifyotp?mobilenumber=162097957&otp=220816";

  login: UserOptions = { username: '', password: '' };
  submitted = false;

  testImage = '';
  public base64ImageCrop = null;
  public base64Image: string;
  public base64Image1: string;
  // tslint:disable-next-line: variable-name
  public mykad_image: string;
  // tslint:disable-next-line: variable-name
  public selfie_image: string;
  public imageSuccess = false;

  
  public phonenumber: string="";
  public otpnumber: string;


  // tslint:disable-next-line: variable-name
  get_name = '';
  // tslint:disable-next-line: variable-name
  get_nric = '';

  signupform: FormGroup;
  userData = {
    name: '',
    document_no: '',
    phone_no: '',
    agent_name: '',
    agent_phone_no: '',
    image: '',
    response: '',
    cb: false,
    cb1: false
  };

  imageURI: any;
  imageFileName: any;

  hideMe = false;
  hideMe1 = false;
  livenessPass = false;
  livenessPass2 = false;

  hidePreview = false;

  passAll = false;
  passAll2 = false;

  

  otpsuccess: boolean =false;
  otpfailed: boolean =true;

  showResult = false;
  ocrPass = false;
  ocrPass2 = false;

  matchPass = false;
  matchPass2 = false;
  livenessloading = false;
  loadingImage = false;
  isLoading = false;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    public router: Router,
    public menuCtrl: MenuController,
    public http: HttpClient,
    public alertCtrl: AlertController,
    public loadingController: LoadingController,
    public storage: Storage,
    private cameraPreview: CameraPreview,
    private file: File

  ) {

  }
  onSlideChangeStart(event) {
    event.target.isEnd().then(isEnd => {
      this.showSkip = !isEnd;

    });

    const me = this;
    me.slides.isEnd().then((istrue) => {
      console.log(istrue);
      if (istrue) {
        this.continue = true;
      } else {
        this.continue = false;
      }
    });

  }


  public items: Array<any>;

  ionViewDidLoad() {
       this.items = [
           { title: 'Notatka 1', description: 'Opis notatki 1' },
           { title: 'Notatka 2', description: 'Opis notatki 2' },
           { title: 'Notatka 3', description: 'Opis notatki 3' }
       ];
   }

  swipeBack() {
    this.slides.slidePrev();
  }

  swipeNext1() {
    this.slides.lockSwipes(false).then(() => {
      this.slides.slideTo(SLIDES_INDEX.CARD_PHOTO_SLIDE, 0).then(() => {
        this.startMyCardCamera();
        this.slides.lockSwipes(true);
      });
    });
  }

  swipeNext2() {
    this.slides.lockSwipes(false).then(() => {
      this.slides.slideTo(SLIDES_INDEX.SELFIE_PHOTO_SLIDE, 0).then(() => {
        this.getSelfie();
        this.slides.lockSwipes(true);
      });
    });
  }

  
  swipeNext3() {
    this.slides.lockSwipes(false).then(() => {
      this.slides.slideTo(SLIDES_INDEX.SENDING_VERIFY_SLIDE, 0).then(() => {
        this.getresult();
        this.slides.lockSwipes(true);
      });
    });
  }


  swipeNext4() {
    this.slides.lockSwipes(false).then(() => {
      this.slides.slideTo(SLIDES_INDEX.TAC_SLIDE, 0).then(() => {
      
        this.slides.lockSwipes(true);
      });
    });
  }


  swipeNext5() {
    this.slides.lockSwipes(false).then(() => {
      this.slides.slideTo(SLIDES_INDEX.TAC_VERIFY_SLIDE, 0).then(() => {
        this.getsms();
        this.slides.lockSwipes(true);
      });
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Click the backdrop to dismiss early...',
      translucent: true,
      cssClass: 'custom-class custom-loading',
      backdropDismiss: true
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed with role:', role);
  }

  onSignup() {
    this.router.navigateByUrl('/signup');
  }



  restartverify() {
    window.location.reload();
    }


  
  
 goBack() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

    
 goNext() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }


  ionViewWillEnter() {
    this.storage.get('ion_did_verification').then(res => {
      if (res === true) {
        this.router.navigateByUrl('/app/tabs/home', { replaceUrl: true });
      }

    });

    this.menuCtrl.enable(false);
    this.slides.lockSwipes(true);

    this.platform.ready().then(() => {
      this.cameraPreview.stopCamera();
    });
  }

  ionViewWillLoad() {
    document.getElementsByTagName('html')[0].style.visibility = 'hidden';
  }


  IonViewWillLeave() {
    document.getElementsByTagName('html')[0].style.visibility = 'visible';
    this.cameraPreview.stopCamera();
  }



  // Naim
  /**
   * Start MyCard Camera
   */
  async startMyCardCamera() {
    await this.cameraPreview.startCamera(this.myCardCameraOption).then((res) => {
      this.statusBar.hide();
      this.isCameraActive = true;
      this.isMyCardCameraActive = true;
    }).catch(error => console.log(error));
  }

  /**
   * Re Capture MyCard
   */
  reCaptureMyCard() {
    this.slides.lockSwipes(false).then(() => {
      this.slides.slideTo(SLIDES_INDEX.CARD_PHOTO_SLIDE, 0).then(() => {
        this.startMyCardCamera();
        this.slides.lockSwipes(true);
      });
    });
  }


  reCaptureMySelfie() {
    this.slides.lockSwipes(false).then(() => {
      this.slides.slideTo(SLIDES_INDEX.SELFIE_PHOTO_SLIDE, 0).then(() => {
        this.getSelfie();
        this.slides.lockSwipes(true);
      });
    });
  }

  /**
   * Switch flash mode
   * ON/OFF
   */
  async switchFlashMode(event: Event) {
    event.stopPropagation();
    event.preventDefault();

    this.isCameraFlashMode = !this.isCameraFlashMode;
    await this.cameraPreview.setFlashMode(this.isCameraFlashMode ? this.cameraPreview.FLASH_MODE.ON : this.cameraPreview.FLASH_MODE.OFF);
  }

  /**
   * Switch camera mode
   * rear/front
   */
  switchCamera(event: Event) {
    event.stopPropagation();
    event.preventDefault();

    console.log('switchCamera');
    this.cameraPreview.switchCamera().then(async () => {
      this.isCameraFront = !this.isCameraFront;
      if (!this.isCameraFront) {
        await this.cameraPreview.setFlashMode(
          this.isCameraFlashMode
            ? this.cameraPreview.FLASH_MODE.ON
            : this.cameraPreview.FLASH_MODE.OFF
        );
      }
    });
  }

  /**
   * Take button
   */
  takePictureButton(event: Event, type: string) {
    event.stopPropagation();
    event.preventDefault();

    if (type === 'mycard') {
      this.takePictureMyCard();
    }
  }

  /**
   * Take picture MyCard
   */
  takePictureMyCard() {
    this.cameraPreview.takePicture(this.myCardPictureOpts).then(async (imageData) => {
      this.myCardBase64 = `data:image/jpeg;base64,${imageData}`;
      this.mykad_image = imageData;
      console.log("NRIC"+  this.myCardBase64 );
      console.log("NRIC"+imageData);
      const boxAreaPosition = this.getBoxAreaPosition('card');
      console.log("BAP:"+JSON.stringify(boxAreaPosition));
      this.isCameraActive = !this.isCameraActive;

      this.cameraPreview.stopCamera().then(async () => {
        setTimeout(() => {
          this.base64ImageCrop = this.cropImage2(this.myCardBase64, boxAreaPosition, 'card');
          console.log("NRICCrop: "+ this.base64ImageCrop);
          this.slides.lockSwipes(false).then(() => {
            this.slides.slideTo(SLIDES_INDEX.MYKAD_SLIDE).then(() => {
              this.isMyCardCameraActive = false;
              this.statusBar.show();
              this.slides.lockSwipes(true);
            });
          });
        });
      });
    }, (err) => {
      console.log(err);
    });
  }

  cropImage2(imgBase64: string, boxAreaPosition, type: string) {
    const canvas: any = document.getElementById(type + '-canvas');

    const canvasRef = canvas.getBoundingClientRect();
    const ctx = canvas.getContext('2d');

    const img: any = document.getElementById(type + '-img');

    const imgRef = img.getBoundingClientRect();
    const nWidth = img.naturalWidth;
    const nHeight = img.naturalHeight;

    ctx.drawImage(
      img,
      (boxAreaPosition.left - imgRef.left) * nWidth / img.width,
      (boxAreaPosition.top - imgRef.top) * nHeight / img.height,
      boxAreaPosition.width * nWidth / img.width,
      boxAreaPosition.height * nHeight / img.height,
      0,
      0,
      canvasRef.width,
      canvasRef.height
    );

    console.log(
      (boxAreaPosition.left - imgRef.left) * nWidth / img.width,
      (boxAreaPosition.top - imgRef.top) * nHeight / img.height,
      boxAreaPosition.width * nWidth / img.width,
      boxAreaPosition.height * nHeight / img.height,
      0,
      0,
      canvasRef.width,
      canvasRef.height,
    );

    return canvas.toDataURL('image/png', 1.0);
  }

  cropSelfie(imgBase64: string, boxAreaPosition, type: string) {
    const canvas: any = document.getElementById(type + '-canvas');

    const canvasRef = canvas.getBoundingClientRect();
    const ctx = canvas.getContext('2d');

    const img: any = document.getElementById(type + '-img');

    const imgRef = img.getBoundingClientRect();
    const nWidth = img.naturalWidth;
    const nHeight = img.naturalHeight;

    ctx.drawImage(
      img,
      (boxAreaPosition.left - imgRef.left) * nWidth / img.width,
      (boxAreaPosition.top - imgRef.top) * nHeight / img.height,
      boxAreaPosition.width * nWidth / img.width,
      boxAreaPosition.height * nHeight / img.height,
      0,
      0,
      canvasRef.width,
      canvasRef.height
    );

    console.log(
      (boxAreaPosition.left - imgRef.left) * nWidth / img.width,
      (boxAreaPosition.top - imgRef.top) * nHeight / img.height,
      boxAreaPosition.width * nWidth / img.width,
      boxAreaPosition.height * nHeight / img.height,
      0,
      0,
      canvasRef.width,
      canvasRef.height,
    );

    return canvas.toDataURL('image/png', 1.0);
  }

  getBoxAreaPosition(type: string) {
    const card = document.getElementById(type + '-area-element');
    const rect = card.getBoundingClientRect();

    return rect;
  }


  takePicture(test: string) {

    if (test == '1') {

      /*this.camera.getPicture({
        quality: 100,
        sourceType: this.camera.PictureSourceType.CAMERA,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        allowEdit: true,
        mediaType: this.camera.MediaType.PICTURE,
        cameraDirection: this.camera.Direction.BACK,
        correctOrientation: true
      }).then((imageData) => {

        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.mykad_image = imageData;
        console.log("MYKAD: " + imageData);

        this.imageSuccess = true;
        this.hideMe = true;
        this.userData.cb = true;

      }, (err) => {
        console.log(err);
      });*/
    } else {
      this.imageSuccess = false;
      this.hideMe1 = false;
      this.userData.cb1 = false;
      this.hidePreview = true;
      console.log('camera 3');
      setTimeout(() => {
        this.getSelfie();
      }, 200);





      /*this.cameraPreview.start({
        parent: 'cameraPreview', position: 'front', height: 100,
        width: 100
      });*/

      this.imageSuccess = false;
      this.hideMe1 = false;
      this.userData.cb1 = false;
      this.hidePreview = true;
      this.slides.lockSwipes(true);



    }
  }

  getSelfie() {

    
    this.hidePreview = true;
    console.log('camera 4');

    const options = {
      x: 0,
      y: 0,
      width: window.screen.width,
      height: window.screen.height,
      camera: this.cameraPreview.CAMERA_DIRECTION.FRONT,
      toBack: true,
      tapPhoto: true,
      tapFocus: false,
      previewDrag: false,
      storeToFile: false,
      disableExifHeaderStripping: false
    };

    this.cameraPreview.startCamera(options).then(
      (res) => {
        console.log(res);
        // alert("success : " + res);
      },
      (err) => {
        console.log(err);
        // alert("Error : " + err);
      });

    /*
        CameraPreview.start({parent: "cameraPreview", className: 'crop', position: "front", height: 1280, width: 720}) .then(() => {
         this.imageSuccess = false;
         this.hideMe1 = false;
         this.userData.cb1 = false;
         this.hidePreview= true;
        }, error => {
         console.log("Check: "+error);
         this.errorhandling();
       });
    */
  }


  errorhandling() {
    setTimeout(() => {

    }, 2000);

    console.log('ErrorHANDLING');
    console.log('camera 5');
    const cameraPreviewOpts: CameraPreviewOptions = {
      x: 0,
      y: 0,
      width: window.screen.width,
      height: window.screen.height,
      camera: 'rear',
      tapPhoto: true,
      previewDrag: true,
      toBack: true,
      alpha: 1
    };

    // start camera
    this.cameraPreview.startCamera(cameraPreviewOpts).then(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.log(err);
      });
    this.imageSuccess = false;
    this.hideMe1 = false;
    this.userData.cb1 = false;
    this.hidePreview = true;
    this.slides.lockSwipes(true);

  }

  async captureSelfie() {


    this.cameraPreview.takePicture().then((imageData) => {
      this.cameraPreview.stopCamera();
      this.base64Image1 = 'data:image/jpeg;base64,' + imageData;
      this.selfie_image = imageData;
      this.slides.lockSwipes(false).then(() => {
        this.slides.slideTo(SLIDES_INDEX.SELFIE_SLIDE).then(() => {
          this.isMyCardCameraActive = false;
          this.statusBar.show();
          this.slides.lockSwipes(true);
        });
      });




      this.imageSuccess = true;
      this.hideMe1 = true;
      this.userData.cb1 = true;
      this.hidePreview = false;




      // alert(imageData);
    }, (err) => {
      console.log(err);
      // alert(err);
    });


    /*
    const result = await CameraPreview.capture();
    const base64PictureData = result.value;
    this.base64Image1="data:image/jpeg;base64," + base64PictureData;
    this.selfie_image =base64PictureData;
    console.log("SELFIE: "+base64PictureData);
    CameraPreview.stop();
  */
  }


  calculateImageSize(base64String) {
    let padding;
    let inBytes;
    let base64StringLength;
    if (base64String.endsWith('==')) { padding = 2; } else if (base64String.endsWith('=')) { padding = 1; } else { padding = 0; }

    base64StringLength = base64String.length;
    console.log(base64StringLength);
    inBytes = (base64StringLength / 4) * 3 - padding;
    console.log(inBytes);
    this.kbytes = inBytes / 1000;
    console.log('Photo size: ' + this.kbytes);
    return this.kbytes;
  }


  DeletePicture(test: string) {
    if (test == '1') {
      this.base64Image = '';
      this.mykad_image = '';
      this.imageSuccess = false;
      this.hideMe = false;
      this.userData.cb = false;
    } else {
      this.base64Image1 = '';
      this.selfie_image = '';
      this.imageSuccess = false;
      this.hideMe1 = false;
      this.userData.cb1 = false;
    }

  }




  gotoNext()
  {
   //this.router.navigateByUrl('/tutorial').then(() => this.storage.set('name', this.get_name)&&this.storage.set('nric', this.get_nric)&&this.storage.set('ion_did_verification', true)&&//this.storage.set('selfie', this.selfie_image ));

    this.router.navigateByUrl('/tutorial').then(() => this.storage.set('ion_did_verification', true)&&this.storage.set('selfie', this.selfie_image ));
  }



  getresult() {

    this.loadingImage = true;
    this.showResult = true;
    const headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    // const requestOptions = new RequestOptions({ headers: headers });

    const myData = JSON.stringify({ app_id: 'liveness001', selfy: this.selfie_image, nric_data: this.base64ImageCrop });



    this.get_name = 'Selfie: ' + this.selfie_image;

    console.log(myData);
    const senddata = myData.replace('[', '');
    const senddata2 = senddata.replace('data:image/png;base64,', '')
    const submit = senddata2.replace(']', '');

    console.log(submit);
    const yourheaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    const request = {
      headers: yourheaders
    };

    this.http.post(this.combineAPI, submit, request)
      .subscribe((data: any) => {
        console.log(data);
        console.log(data._body);
        this.loadingImage = false;
        const a = JSON.parse(JSON.stringify(data));
        this.get_name = a.name;
        this.get_nric = a.nric;
        console.log('liveFaceDetected: ' + a.liveFaceDetected);
        console.log('name: ' + a.name);
        console.log('nric: ' + a.nric);
        console.log('ocrFaceDetected: ' + a.ocrFaceDetected);
        console.log('photoMatch: ' + a.photoMatch);
        console.log('photoMatchScore: ' + a.photoMatchScore);
        console.log('spoofConfidence: ' + a.spoofConfidence);
        if (a.liveFaceDetected == true && a.spoofConfidence < (0.4)) {
          console.log('User Liveness Pass');
          this.livenessPass = true;
          this.livenessPass2 = false;

        } else {
          this.livenessPass = false;
          this.livenessPass2 = true;
          console.log('Liveness Not Passed');

        }


        if (a.photoMatch == true) {
          console.log('Match Pass');
          this.matchPass = true;
          this.matchPass2 = false;
        } else {
          this.matchPass = false;
          this.matchPass2 = true;
          console.log('Match Not Passed');

        }

        if (a.ocrFaceDetected == true) {
          console.log('OCR Pass');
          this.ocrPass = true;
          this.ocrPass2 = false;
          // this.getmatch();

        } else {
          this.ocrPass = false;
          this.ocrPass2 = true;
          console.log('OCR Not Passed');

        }


        if (a.liveFaceDetected == true && a.photoMatch == true && a.ocrFaceDetected == true && a.spoofConfidence < (0.4)) {
          this.passAll = true;
          this.passAll2 = false;
        } else {
          this.passAll = false;
          this.passAll2 = true;
        }


      }, error => {
        this.loadingImage = false;
        console.log('Oooops!' + error);

        this.matchPass = false;
        this.matchPass2 = true;
        console.log('Match Not Passed');
        this.livenessPass = false;
        this.livenessPass2 = true;
        console.log('Liveness Not Passed');
        this.ocrPass = false;
        this.ocrPass2 = true;
        console.log('OCR Not Passed');
        this.passAll = false;
        this.passAll2 = true;
      });


  }


  
  async getsms()
  
  {  
    console.log(this.phonenumber.length);
   if(this.phonenumber.length<8)
   {

    const alert = await this.alertCtrl.create({
      header: 'Warning',
      message: 'Please enter Valid Mobile Number',
      buttons: ['OK']
      
  });
  alert.present();
   }
   else{
    
    
    let yourheaders = new HttpHeaders({
      "Accept":'application/json',
      'Content-Type': 'application/json',
      });
  
      let request = {
      headers: yourheaders
      }
  this.http.get(this.otp+this.phonenumber, request)
    .subscribe(async data => {
      console.log(data); 
      console.log(data["_body"]);
      this.loadingImage=false;
      let a = JSON.parse(JSON.stringify(data));
     


     }, async error => {

      console.log("Oooops!" + JSON.stringify(error));
      let b = JSON.parse(JSON.stringify(error));
    });

    const alert = await this.alertCtrl.create({
      header: 'TAC REQUESTED',
      message: 'Check your SMS for TAC Number',
      buttons: ['OK']
      
  });
  alert.present();
  }

  }


  async getsms2()
  
  {  
    console.log(this.phonenumber.length);
   if(this.phonenumber.length<8)
   {

    const alert = await this.alertCtrl.create({
      header: 'Warning',
      message: 'Please enter Valid Mobile Number',
      buttons: ['OK']
      
  });
  alert.present();
   }
   else{
    
    
    let yourheaders = new HttpHeaders({
      "Accept":'application/json',
      'Content-Type': 'application/json',
      });
  
      let request = {
      headers: yourheaders
      }
  this.http.get(this.otp+this.phonenumber, request)
    .subscribe(async data => {
      console.log(data); 
      console.log(data["_body"]);
      this.loadingImage=false;
      let a = JSON.parse(JSON.stringify(data));
     


     }, async error => {

      console.log("Oooops!" + JSON.stringify(error));
      let b = JSON.parse(JSON.stringify(error));
    });


    const alert = await this.alertCtrl.create({
      header: 'TAC REQUESTED',
      message: 'Check your SMS for TAC Number',
      buttons: ['OK']
      
  });
  alert.present();
  }

  }

  
  async getsmsverify()
  
  { 
    
    console.log(this.otpnumber.length);
    if(this.otpnumber.length<6)
    {
 
     const alert = await this.alertCtrl.create({
       header: 'Warning',
       message: 'Please enter Valid TAC Number',
       buttons: ['OK']
       
   });
   alert.present();
    }
    else{
   
    var myData = JSON.stringify({mobile_number:this.phonenumber});
  
    console.log(myData);

    
    let yourheaders = new HttpHeaders({
      "Accept":'application/json',
      'Content-Type': 'application/json',
      });
  
      let request = {
      headers: yourheaders
      }
  this.http.get("https://cors-anywhere.herokuapp.com/http://18.141.110.210/verifyotp?mobilenumber="+this.phonenumber+"&otp="+this.otpnumber, request)
    .subscribe(async data => {
      console.log(data); 
      console.log(data["_body"]);
      this.loadingImage=false;
      let a = JSON.parse(JSON.stringify(data));
  
      console.log(a.verified); 
      if (a.verified==true)
      {

      this.gotoNext();

      }
      else
      {
        this.otpsuccess = false;
        this.otpfailed = true;
        const alert = await this.alertCtrl.create({
          header: 'Invalid',
          message: 'TAC Verification Failed',
          buttons: ['OK']
          
      });
      alert.present();
      }
      

     }, async error => {

      console.log("Oooops!" + JSON.stringify(error));
      let b = JSON.parse(JSON.stringify(error));
    });
  }
  }




  /*
  getmatch()

  {

    this.showResult = true;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });

    var myData = JSON.stringify({caller:"Mobile",  base64_source: this.selfie_image, base64_target: this.selfie_image});
  console.log(myData);

  this.http.post(this.matchURL, myData, requestOptions)
    .subscribe(data => {
      console.log(data);
      console.log(data["_body"]);

      let a = JSON.parse(data["_body"]);



      if (a.IsMatched ==true){
        console.log("Match Pass");
        this.matchPass = true;
        this.matchPass2 = false;
        //this.getocr();
      }
      else
      {
        this.matchPass = false;
        this.matchPass2 = true;
        console.log("Match Not Passed");
      }


     }, error => {

      this.matchPass = false;
      this.matchPass2 = true;
      console.log("Oooops!" + error);
    });


  }*/



  /*
 getliveness()
   {

     this.showResult = true;

     var headers = new Headers();
     headers.append("Accept", 'application/json');
     headers.append('Content-Type', 'application/json' );
     const requestOptions = new RequestOptions({ headers: headers });

   var myData = JSON.stringify({app_id:"liveness001" , frontimage: this.selfie_image});

     //var myData = JSON.stringify({email:"pdrm.officer@emgs.com.my" , password: "abc123@@"});

     console.log(myData);


     this.http.post(this.livenessURL, myData, requestOptions)
       .subscribe(data => {
         console.log(data);
         console.log(data["_body"]);

         let a = JSON.parse(data["_body"]);


         console.log(a.faceDetected);

         if (a.faceDetected =="1"){
           console.log("User Liveness Pass");
           this.livenessPass = true;
           this.livenessPass2 = false;

          //this.getocr();


         }
         else
         {
           this.livenessPass = false;
           this.livenessPass2 = true;
           console.log("Not Passed");
         }


        }, error => {
         this.livenessPass = false;
         this.livenessPass2 = true;

         console.log("Oooops!" + error);
       });


   }*/
  /*
  getocr()
  {
    this.loadingImage=true;
    this.showResult = true;
  var headers = new Headers();
  headers.append("Accept", 'application/json');
  headers.append('Content-Type', 'application/json' );
  const requestOptions = new RequestOptions({ headers: headers });


            var myData = JSON.stringify({
              frontimage: this.mykad_image,
              backimagecheck: false,
              backimage: "",
              rgbcheck: true,
              clientid: "1",
              scanid: "1",
              mykid: false
          });
  console.log(myData);

  this.http.post(this.ocrURL, myData, requestOptions)
    .subscribe(data => {
      console.log(data);
      console.log(data["_body"]);
      this.loadingImage=false;
      let a = JSON.parse(data["_body"]);
      //this.loadingImage=false;
      console.log(a.name);
      this.get_name = a.name;
      this.get_nric = a.nric ;



      if (a.liveFaceDetected ==true){
        console.log("OCR Pass");
        this.ocrPass = true;
        this.ocrPass2 = false;
        //this.getmatch();
      }
      else if (a.liveFaceDetected ==true){
        console.log("OCR Pass");
        this.ocrPass = true;
        this.ocrPass2 = false;
        //this.getmatch();
      }
      else if (a.liveFaceDetected ==true){
        console.log("OCR Pass");
        this.ocrPass = true;
        this.ocrPass2 = false;
        //this.getmatch();
      }
      else
      {
        this.ocrPass = false;
        this.ocrPass2 = true;
        console.log("OCR Not Passed");
      }


     }, error => {
      this.loadingImage=false;
      this.ocrPass = false;
      this.ocrPass2 = true;
      console.log("Oooops!" + error);
    });


  }*/

}
