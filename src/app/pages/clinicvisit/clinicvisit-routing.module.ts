import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClinicVisitPage } from './clinicvisit';

const routes: Routes = [
  {
    path: '',
    component: ClinicVisitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClinicVisitPageRoutingModule { }
