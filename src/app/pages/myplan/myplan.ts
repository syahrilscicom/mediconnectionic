import { Component } from '@angular/core';
import { PopoverController, MenuController } from '@ionic/angular';
import { PopoverPage } from '../about-popover/about-popover';
import { Router } from '@angular/router';
import { BarcodeScannerOptions,  BarcodeScanner} from "@ionic-native/barcode-scanner/ngx";
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'page-myplan',
  templateUrl: 'myplan.html',
  styleUrls: ['./myplan.scss'],
})
export class MyPlanPage {
  location = 'madison';
  conferenceDate = '2047-05-17';
  get_name: string = "";
  public base64Image1: string;
  public base64Image1null: string;
  selectOptions = {
    header: 'Select a Location'
  };
  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  employee: boolean;
  dependant: boolean;
  employeeimage: boolean;
  segment = 'all';

  constructor(public popoverCtrl: PopoverController,
    public menuCtrl: MenuController, public router: Router, private barcodeScanner: BarcodeScanner,  public storage: Storage,
    private statusBar: StatusBar
  ) {
   
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };

    this.showEmployee();
  }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }


  showEmployee() {
    var promise1= this.storage.get('name');
    
    Promise.all([promise1]).then((arrayOfResults) => {
    console.log(arrayOfResults[0]);
    this.get_name = arrayOfResults[0];
    });
    this.employee = true;
    this.dependant = false;

    var promise1= this.storage.get('selfie');
    
    Promise.all([promise1]).then((arrayOfResults) => {
    console.log("Image: "+arrayOfResults[0]);

    if (arrayOfResults[0]==null)
    {
      this.employeeimage=true;
    }
    else{

      this.employeeimage=false;
    this.base64Image1 =  "data:image/jpeg;base64," +arrayOfResults[0];
  }
    });


  }

  showDependant() {
    this.dependant = true;
    this.employee = false;
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  this.statusBar.backgroundColorByHexString('#2f90b0');
    console.log("Choose My Plan!");
    this.segment = 'all';
    this.showEmployee();
  }

  gotoMyCard()
  {    
    //this.router.navigateByUrl('/mycard');
    
    this.router.navigateByUrl('/mycard').then(() => this.storage.set('plantype', "Employee1"));

  }

  gotoMyCard2()
  {
    
    this.router.navigateByUrl('/mycard').then(() => this.storage.set('plantype', "Dependant1"));
  }

  gotoMyCard3()
  {
    
    this.router.navigateByUrl('/mycard').then(() => this.storage.set('plantype', "Dependant2"));
  }



}
