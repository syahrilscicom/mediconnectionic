import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CaptureSelfiePage } from './captureselfie';

const routes: Routes = [
  {
    path: '',
    component: CaptureSelfiePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaptureSelfiePageRoutingModule { }
