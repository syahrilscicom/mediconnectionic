import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClaimsDetailPage } from './claims-detail';

const routes: Routes = [
  {
    path: '',
    component: ClaimsDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClaimsDetailPageRoutingModule { }
