import { Component } from '@angular/core';

import { ConferenceData } from '../../providers/claims-data';
import { ActivatedRoute } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'page-claims-detail',
  styleUrls: ['./claims-detail.scss'],
  templateUrl: 'claims-detail.html'
})
export class ClaimsDetailPage {
  claims: any;
  isFavorite = false;
  defaultHref = '';
  speakers: any[] = [];
  constructor(
    private dataProvider: ConferenceData,private navCtrl: NavController,
    private userProvider: UserData,
    private route: ActivatedRoute
  ) { }

  goBack() {
    this.navCtrl.back();
    }

  ionViewWillEnter() {
    this.dataProvider.load().subscribe((data: any) => {
      if (data && data.schedule && data.schedule[0] && data.schedule[0].groups) {
        const claimsId = this.route.snapshot.paramMap.get('SessionId');
        for (const group of data.schedule[0].groups) {
          if (group && group.claimss) {
            for (const claims of group.claimss) {
              if (claims && claims.id === claimsId) {
                this.claims = claims;

                this.isFavorite = this.userProvider.hasFavorite(
                  this.claims.name
                );

                break;
              }
            }
          }
        }
      }
    });
  }

  ionViewDidEnter() {
   // this.defaultHref = `/app/tabs/home`;

    this.dataProvider.getSpeakers().subscribe((speakers: any[]) => {
      this.speakers = speakers;
    });
  }

  claimsClick(item: string) {
    console.log('Clicked', item);
  }

  toggleFavorite() {
    if (this.userProvider.hasFavorite(this.claims.name)) {
      this.userProvider.removeFavorite(this.claims.name);
      this.isFavorite = false;
    } else {
      this.userProvider.addFavorite(this.claims.name);
      this.isFavorite = true;
    }
  }

  shareClaims() {
    console.log('Clicked share claims');
  }
}
