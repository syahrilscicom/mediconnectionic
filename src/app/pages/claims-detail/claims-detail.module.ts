import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClaimsDetailPage } from './claims-detail';
import { ClaimsDetailPageRoutingModule } from './claims-detail-routing.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ClaimsDetailPageRoutingModule
  ],
  declarations: [
    ClaimsDetailPage,
  ]
})
export class ClaimsDetailModule { }
