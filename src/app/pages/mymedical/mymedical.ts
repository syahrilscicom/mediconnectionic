import { Component } from '@angular/core';
import { PopoverController, MenuController } from '@ionic/angular';
import { PopoverPage } from '../about-popover/about-popover';
import { Router } from '@angular/router';
import { BarcodeScannerOptions,  BarcodeScanner} from "@ionic-native/barcode-scanner/ngx";
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'page-mymedical',
  templateUrl: 'mymedical.html',
  styleUrls: ['./mymedical.scss'],
})
export class MyMedicalPage {
  location = 'madison';
  conferenceDate = '2047-05-17';
  get_name: string = "";
  get_nric: string = "";
  selectOptions = {
    header: 'Select a Location'
  };
  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;


  constructor(public popoverCtrl: PopoverController,
    public menuCtrl: MenuController, public router: Router, private barcodeScanner: BarcodeScanner, public storage: Storage,
    private statusBar: StatusBar
  ) {
    //this.encodeData = "ABC123";
    //Options
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.statusBar.backgroundColorByHexString('#2f90b0');
    var promise1= this.storage.get('name');
    
    Promise.all([promise1]).then((arrayOfResults) => {
    console.log(arrayOfResults[0]);
    this.get_name = arrayOfResults[0];
    });


    var promise2= this.storage.get('nric');
    
    Promise.all([promise2]).then((arrayOfResults) => {
    console.log(arrayOfResults[0]);
    this.get_nric= arrayOfResults[0];
    });
  }

  scanCode() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        alert("Barcode data " + JSON.stringify(barcodeData));
        this.scannedData = barcodeData;
      })
      .catch(err => {
        console.log("Error", err);
      });
  }
  /*encodedText() {
    this.barcodeScanner
      .encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeData)
      .then(
        encodeData => {
          console.log(encodeData);
          this.encodeData = encodeData;
        },
        err => {
          console.log("Error occured : " + err);
        }
      );
  }*/

  encodedText(){
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE,this.encodeData).then((encodedData) => {

        console.log(encodedData);
        this.encodeData = encodedData;

    }, (err) => {
        console.log("Error occured : " + err);
    });                 
}
}
