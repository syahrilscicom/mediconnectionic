import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { PopoverPage } from '../about-popover/about-popover';
import { MenuController, Platform, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { NavController} from '@ionic/angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  styleUrls: ['./home.scss'],
})
export class HomePage {
  location = 'madison';
  get_name: string = "";
  public base64Image1: string;
  public base64ImageDefault: string;
  employeeimage: boolean;
  get_response: string = "";
  selectOptions = {
    header: 'Select a Location'
  };

  constructor(    public alertCtrl: AlertController, public popoverCtrl: PopoverController,public menuCtrl: MenuController,    public router: Router, 
    private platform: Platform,    public storage: Storage, private statusBar: StatusBar,    private nav: NavController, 
    private http:HttpClient) { }




  ionViewWillEnter() {



    this.statusBar.styleDefault();
    this.statusBar.overlaysWebView(false);
  this.statusBar.backgroundColorByHexString('#24B9EB');
    

    // let status bar overlay webview
//this.statusBar.overlaysWebView(true);

    this.menuCtrl.enable(true);
    var promise1= this.storage.get('name');
    
    Promise.all([promise1]).then((arrayOfResults) => {
    console.log(arrayOfResults[0]);
    this.get_name = arrayOfResults[0];
    });




    var promise1= this.storage.get('selfie');
    
    Promise.all([promise1]).then((arrayOfResults) => {
    console.log(arrayOfResults[0]);
  

    if (arrayOfResults[0]==null)
    {
      this.employeeimage=true;

      this.base64ImageDefault =  "data:image/jpeg;base64,"+"iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABmJLR0QA/wD/AP+gvaeTAAARSUlEQVRYCe2ZC3RVxRWGz7lJgEREgtaKJIg8giLGJSpVoRArsnStIoqi4ZFHleKraa1vFOoTBa1aRdSmkiaBJCiIVF1tbbSNCqgUBaGiQAK2BKsWEkHIg5DcfoM3Bc/dcx73kVxXb9a/c8759549c/aemTMz1zDif/EIxCMQj0A8AvEIxCMQj8D/YwTMWHvpiRMn9urSpcsPTNM8mbYNDkg6V4VU9e8wqQ/c/4vrJsps8vv9Hzc3N69esmRJHVzMo9MTQMATkpKSRiUkJIwleGOI2DDEh4SDVgp/gFSSlL80NTWtICGKg4otdEoC7rnnHt/WrVt/1NramkuAxhGSnkg0UU9yXyXJpf379/8r9bdFszIvvjs0Aby4r6am5hKCMZNGno50BtZS//1lZWXLqdyPdCo6KgHm1KlTJ/Kms5ChSCxgA6Pv/oULFy7pzMZEPQFTpkw5mxedz0uquZ1LbIG2vU+LbiAR73HtcEQtAXxck7t27TqbNypAEhEv+IJpYiUFNvp8vo/4VnzC/R7m8L3ct3BvcJ/EfXfue2BzMvZDuB9CQEdyPRbxAuXzicTExFnFxcVNXgqGaxuVBOTm5ma0tbW9QONOQ9ygFaM3TdNcRrm/MT9v5DlUmHl5eUNIznmmaU4gMaNx5HZVtRbbKxYtWlTNtUMQ8QQw5VzJi/+O1h+JOGErtk+1tLQ8v3jx4s+cjEPR0xn6kIxs6rmB8iciTthD0q6mEyx1MoyE3oyEk3YffGjv5P4BxNYvwfiEl3yotra2vKqq6gD2Ucf06dOTGhsbp1DvDCrLQOzgp4138F142M4oEjozEk7U8nLLli3zafS1Dv52or+ZIb6Qqx/pDKgVWQ4VP4Ycjdjh6YEDBxbwflHbNyTY1e5SZ6anpz+H7U8RO7x04MCBi8vLy1fYGXWEbv369R+eeuqp5dQ1gE5zElcdzqqrq0vD/hWdQbh82COAaedZGnENosNeFDfQ60u5xhxofy6NUsvk7lx1eIb2X69ThsOHNQJycnJmUvmtiA7b6GFjFi1aVKkz6Gye3v1hZmbmH/k2XEhbe2racxYjpnnDhg0RH70hjwBWO5NpcJmmwYpWa/ixpaWlO9RDrEt2dvbx7AP+RDszEQnkyH9FpFdHISWApZ1a56+hleJSk8S8z3x/YUVFxU5svjPIz8/vSbtfpcEjEAm72QCeUVJSUiMpQ+F8XguxnEths/QS5cTgw3/I8e/537Xg026juLj4K36LuIj7tYiEo9hTLFcxkJShcJ4T0NDQcC8VDUEk7CA54zl73y0pvwtcUVHR1xxtXEJbdVPnUPYTs9BHBKYXL8z76qxlHWWSECuamXpGsnlZY1WE+sx5UkK3bt1GMvlejI8zkO8FhIvxH/59iawh6a+wm15J4lt5jggmT548nES8jbMuiBX7ITJZXGziGha8JEBtYFSDxPmR4F9D8AvDak2gsBrijLQbebwJORpxg50k6tH9+/c/QSIa3RRwsqHDXc97zdfYvU0CRqPzIyHDdQJYL19GLUsRCctpzKWSwivH0nYigXyMcmmIZxCw7RS6kc6wjGu4UJ3uDzgZh0gYz3u/LCnccq6+AWzFld19Gqdf8WG6VqNzTavphiQ/TPBfoFBIwaecQfl0ZCm998FAuxUdqvhZmk6n8G5EgoqJ604sOUiQSCuXlpY2Hq4AkXAHxwtvSAq3nArUnj171Oi62m0ZBzsGgvlDjhGGTpgwYSkHfiFPE+vWrdvLRu0A9Y1FrDiODdoaNmibrQq3zz43hvSo2zR21SkpKU9rdK7p6urqX2N8KRJpXIbvueE6bW5ufpIYfGoIf2T6doF2TTkmgGlhGJWcI3mEn11YWNgi6dxy+FffFvXB1Rbh5V9DeRWrnQxWJscoUffwasT8BZ0dbmZFo0awnY2tjo/6fvWuGqORU6dOPU2jc6QTnSyoOIcXlcy2JScnl0kKtxzzfjL+H8e/bh7dSLCncZzxjuBzF9wWpIgAnMv1OeRkxAqqMH9TUFDw53nz5jVblW6fGekl+/btuwtn/YzgvzyomxDPsB0BWVlZiQTnSskrDXk23N7PGr8A/+mSf7g3+QCO0AQf9SGwElmFrUrCW4fYQ3e0tR/fg58dYrzfqXelMxRqSmarWGl0trTPTtu3b98swzB6I1a0svFZZCW9PNP7Ewj+zZoym5liLlVHAxp9EK1smavVd6Q6SAlBEm5RH3tuQ0bgndsEB7379OkzWuAdKdsEEATpy6+cvhbub7hJSUmjcHQsYoWfenNYWdVbFU7PzNV19NKfaOyO27x580iNzhXNO2+n01RKxiT4Aol34mwTgFNdVl9ycuykJ1DjNDaVBH+1RudIM2WtwOgNJAg2dQbZ6gh8LNfodLHSmH9DaxPAyiGVbJ/xjdm3/5MY8QW/beX4dKZkge8KiffC0e5yyR7f4vtItjoO369rdGey+euh0WlpbQL4qKnGJlhL8hLbFy5cuM3Kh/B8nFQG/6sk3gvHFKbzIX3PvLg2+OBXU2AHYkUibR9mJZ2etQkg04OlwvBqiEsqr5wYDI56P/PqyGrP8lgKkDqm6GO1DeWZQOtiIMbMrg67BJwkFaTyjRIfAmdKZVia+iU+ljg64cdSe4iNGDPJtp3TJgCDDETCJokMgftMKsPB3vES74XjSFrnQ6zTi29lS6DFGJCYyI0AKhJfgsrVHIg6bIjTBP6zjDD/8HGexoVYp8bWjtbFQIyZnSO7EXCkVJAfreskPgTuA6kMwbtc4r1w9ESdD7FOL74DtrsCV+ulh5VwetYmgEAcKRVmN/i1xHvlCNIyTZmxLOfO1+gc6dzc3DEYieVZw+vqpIh70Al1MRBjZudZmwAC1F1TcK+G90QzT79LgS+RIJD8+fn5+T2DFA6E2ruwBH1aY/Z5//7939PoPNHHHHNM9BPgqUUhGHNs0EqgH9UUHUwvW855US+NPoim5x9ND/8DikGIhEc4C2qTFJ3JaUcAwdH1dN3I8PweCQkJT1LoX4iE0V27dl1DYNWUIun/xykbev7fIX6ISNiWmpo6X1KEwu3cufNITTndyNCYG0aiTsMUpJz1suo5RFOV11n5UJ45wWzKycn5JXUtpbyJWHEiga3kvL8SRQWdoqpnz54Hl5K7du3qw3OWYRiTsBnDVQe1r7gxnN8CrI6pt4eVCzzvCVxdX3w2lioBQWqOKHoFkWEQHGssIwFzHFxcgL4Iu6319fVNSphuagjEAsQu+Gr3O5vjg5cpHzEwcntpnIkx09gepO0SUHvQwvKPIHjebFhcBD0OGjRoJmTYJ6z4sOJFfN9tJcN9tomBGDO7+uwSELHdnl0DmIKOqKmpuQ6boUikMRTf0/lLibBjXScUY2ZXtzYBDO0tmoK6yjXmMs2KxMd6/yp60xbkKawGIZHGYHw/09DQsInvSK6qM0IViDGwiZm2WrsE6LI5QuvNpYJVSwY98x0avIAivZFoI40KSqqrq9/Oy8sbwH24GCk54Bxrk8TbcdoE8LF9j4ItiBV92fCE3FuZcvJZtXxAzxxuddwBz+cSpHVqNIRa16RJk06irHTms7979+4qZqjdQ5uAoqKir+mhayVXrAJGSbwdp4Y/U85cAv977I5AnNCEQSVtmEWZcVyHkLhehwv8Kcg45G5s30D2I07ojkEJbXmQq7T0hdaDjpllyH9rCgsLG2SVnk3Uqwy1hHsd/XDkWyAI2RALEFfIyspKZPiXEsRJTgUIppqa5nN9paysbI+DfT36jciriMHUdjTlspHpPGciWtCWGSShN6ukq+kcbVpDiwLfV1qo9sfK9hsvV9seQAPPp6EqCVafrRwV9F28ePFnVoX1WQU/LS2tAv5yRAvqWc30cGt5eflbWiMPCpIxho7yOEWGIlpQbxF7kWkY+BFbZGdnpzMCPjUMw4d8C9Q1OpS2Bzk63CsHZlWGYfwbsSKBhuRYSek5PT39EXi74DcQhOsIwtmhvAC+RZSWlr5eW1t7Oj12BgbNiAj0V/FNuFdUWsjAO0sxq83IyFhhMXf1aDpZMQrmEqDbBLvPm5ub+3Oo1ijoDlK8WC43JYgO1QRgPFPNRp1BJHjacRp+XkJORCT4ecfJdILFklJxHAwmcza1jfvvI1Y8yG77Livp5lnKprVciZUIPB/H77d5gfugCy89ENLuAGwlxwlnRzv4tMEgOB8yvQ3nfhUiwaQj/JYj8H6G5q9Lly7TUEnBN0jeInQhwTEBgQC9K3mn0Tezy0yy6vio+dCVwndHJLzJ6LmQaWKXpIwGV1FRsZMgXojvlYiEHnzXVGczrUp6fxe4mxAJKxk5H0sKN5zPjRE29yISBjY2Nt5iVbDJmkavOMfKB543EPwfM3XtDTx32EUtrUnCRVT4D0TCKEZuvmH5o8wdvE8/Q/7TxUa2trCm5Vn7SMNWozwLsaKRxp1CL9imFGy0jqD3b+G+N2JFA8QwpoRNXDsN7IZPYUpaQwO6IVbUpqSkDG5f07Pp7O/z+VTCkq2GPK/iXUZwDRluR4CqYK76J0gyAX/sMP5q7nsjQSBRM2jwpiBFBxMlJSUf0eaZmmrT9u3bl29882ey6XyC22REwlyJ9MK5HgHKKaPgj1wvQoLAC92yY8eOJ1jzV6M8AbFiDcEfDulHOh3qO8VUuZp2nyE0ZhvT5CBWPbegm4NIeJn3GS8pvHBeRoDaGasGtUgV0LtnE/xr0Z2ABAH9TEg/EhMgAW005FeIhBOZ99W73Ccp4ZoYGbqPMmr38DQClFtGgdpYqUSoR6s0QKQgVrxLbznHSsbCM+/zPu0YhlixD+IIJAiMmrtZHeqSE2RvR/jslJKOD9Sd8O8iEqTgq5HzrGQcCxzB1LVNDD5t/ivnRw9wjQg8J4DVQQurgjxq/xpxgybm0mVuDDvDJikpaQn17kfc4CuOIzwd3jk59ZwA5ZAN1GZ6zrXq3kmwW6HW3052naUvLi7+irpXIU7wYzAN+0+NCP6FlABVP3NgOR/WWereTrCpsdPHgs5NG+lId/IdezHS7U0Ix+H69evfyszMTMbHSESH07HxTZgw4a2qqirVi3R2Hc6zEvKxcrubin+B2C1IHqTD3YdNxGFXqdvKTFYSCzD+CaIFPWgZu88CN78haJ1EUMEpbxo9fz4uL0a0wMb17wVaJzYK00bnSUUSbqfAHMQOjSgfTk1NfWjevHnN3Hc4CgoKutbX18+g4tsQNXq5aHEH085crTYCioglQLWFc6AZ9PTZ3Nv6pVd9gt1D/GBSzrR0APuoQ53acnA4hXpV8DMcKvSjv53gP8I1qrANVCg1M7SvJMCFlO2BOGErtk+1tLQ8H62piZ8m+zD1ZdOQn1FXP8P5rx67aRwuLnM2Dd8i4glQTeIEcRB7hSXcn4a4QStGb5qmuYzfVv/GB28jz6HC5LRzCEE/zzTNCfT40TjyIW6wivon89PoP90YR8LGjIQTyQe/LnXjBw41Hf0cfSLiBV8QuJUU2EgiPyKYn7ABqmMK2c3vCLvhDX4kOSo5Ofko6ujFucxJcEMpM4TrCORYxAuaSNbs7du3z+moKbG9cVFLQHsFfBdUYNRqY1Q7F2PXStpzPfN9NdcOR9QTEHgjtVSdyP0sZCgSC1hLI2YT+IhvrvDrGh2VgIMNUhsfzuAvYaqYCXE60hmoYrqZw0f2tc6o3Fpnhybg8MpZLQ0hEFfATUUGINHEFpwvIPF838tquY8ZdFoC2iPAxzSBE8lRfEjHEqAx8Ops3u2qBXMRrbAf4O91PuKvDRgw4G1GXxtczKHTE2CNCAnpxfH1cEbHAAKoRsZA7o/nPhXbduHWqOefWrPXo9vBfQ12NdzX8HPialZLdXBxxCMQj0A8AvEIxCMQj0A8AvEIxCMQj0A8AvEIxCMQj0A8AvEIxCMQMxH4L2bmGUQ9SHpIAAAAAElFTkSuQmCC";
    }
    else{

      this.employeeimage=false;
    this.base64Image1 =  "data:image/jpeg;base64," +arrayOfResults[0];
  }
  
    });

this.getProfile();
    
   }

 

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }



  getProfile()
{

  let yourheaders = new HttpHeaders({
    "Accept": 'application/json',
    'Content-Type': 'application/json'
    });

    let request = {
    headers: yourheaders
    }

  this.http.post("http://dev.ccs.sc/api/user/home", "", request)
    .subscribe(data => {
      console.log(JSON.stringify(data));
     /*this.get_response = JSON.stringify(data);
      let a = JSON.parse(JSON.stringify(data));

      console.log(a.isVerified);

      if (a.isVerified =="true"){
        console.log("User is true");
      }
      else
      {
    
      }*/
     

     }, error => {
      console.log(JSON.stringify(error));
      this.get_response = JSON.stringify(error);
    });
  }


  goToMyPlan() {
   
    this.router.navigateByUrl('/app/tabs/myplan', { replaceUrl: true });
 
  }

  goToUtilization() {
   
    this.router.navigateByUrl('/app/tabs/myplan', { replaceUrl: true });
 
  }

  goToClaim() {
   
    this.router.navigateByUrl('/claims');

 
  }

  goToClinic() {
   
    this.router.navigateByUrl('/clinicvisit');

}
  goToPlan() {
    
      
    this.router.navigateByUrl('/mycard').then(() => this.storage.set('plantype', "Employee1"));


  }

goToNotification() {
   
 // this.router.navigateByUrl('/app/tabs/notification', { replaceUrl: true });

}

}
