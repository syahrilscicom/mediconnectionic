import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { TermsPage } from './terms';
//import { PopoverPage } from '../about-popover/about-popover';
import { TermsPageRoutingModule } from './terms-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermsPageRoutingModule
  ],
  declarations: [TermsPage],
  //entryComponents: [PopoverPage],
  bootstrap: [TermsPage],
})
export class TermsModule {}
