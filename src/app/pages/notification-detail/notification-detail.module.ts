import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationDetailPage } from './notification-detail';
import { NotificationDetailPageRoutingModule } from './notification-detail-routing.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    NotificationDetailPageRoutingModule
  ],
  declarations: [
    NotificationDetailPage,
  ]
})
export class NotificationDetailModule { }
