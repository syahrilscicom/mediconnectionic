import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import { MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';

import { AuthService } from '../services/auth.service';
import { AlertController } from '@ionic/angular';

import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { from } from 'rxjs/internal/observable/from';
import {HTTP} from '@ionic-native/http/ngx';
import { finalize } from 'rxjs/operators';

import { ToastController } from '@ionic/angular';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage  {
  credentials = {
   // username: '',
  //  password: ''
    username: 'MY930617145623@101.sc',
    password: 'abc123@@'
};

  submitted = false;
  get_verification: string = "";
  data: any;
  get_response: string = "";
  constructor(
    public userData: UserData,
    public router: Router,
    public menuCtrl: MenuController,
    public storage: Storage,
    private auth: AuthService,
    private alertCtrl: AlertController,
    private nativeCall: HTTP,
    private http:HttpClient,
    private loadingCtrl:LoadingController,
    public toastController: ToastController


  ) { }

  login()
  {
      this.auth.login(this.credentials).subscribe(async res => {
          if (res) {
          //  this.presentToast();
              this.router.navigateByUrl('/loginverify');

              
          } else {
              const alert = await this.alertCtrl.create({
                  header: 'Login Failed',
                  message: 'Wrong credentials',
                  buttons: ['OK']
              });
              alert.present();
          }
      });
  }
  onLogin(form: NgForm) {
    this.submitted = true;
    this.storage.set('name', "")&&this.storage.set('nric', "")&&this.storage.set('ion_did_verification', false);

    if (form.valid) {
 this.userData.login(this.credentials.username);

   this.storage.set('name', "Syah")&&this.storage.set('nric', "8709")&&this.storage.set('ion_did_verification', false);

   this.router.navigateByUrl('/loginverify');

/*
 
 this.auth.login(this.credentials).subscribe(async res => {
        if (res) {
          this.userData.login(this.credentials.username);

          var promise1= this.storage.get('ion_did_verification');
    
          Promise.all([promise1]).then((arrayOfResults) => {
          console.log("Verification: "+arrayOfResults[0]);
         
          });

          this.router.navigateByUrl('/loginverify');

          
        } else {
            const alert = await this.alertCtrl.create({
                header: 'Login Failed',
                message: 'Wrong credentials',
                buttons: ['OK']
            });
            alert.present();
        }
    }, async error => {
      console.log("Network: "+JSON.stringify(error));
      this.get_response = JSON.stringify(error);

      const alert = await this.alertCtrl.create({
        header: 'Login Failed',
        message: 'API Connection Failed',
        buttons: ['OK']
    });
    alert.present();
    });
*/
  
    }
  }


  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Your settings have been saved.',
      duration: 2000
    });
    toast.present();
  }

  
getlogin()
{

  let yourheaders = new HttpHeaders({
    "Accept": 'application/json',
    'Content-Type': 'application/json'
    });

    let request = {
    headers: yourheaders
    }

  var myData = JSON.stringify(this.credentials);
  // var myData = JSON.stringify({email:"pdrm.officer@emgs.com.my" , password: "abc123@@"});

  // this.http.post("https://cors-anywhere.herokuapp.com/http://mobileapp.emgs.com.my/api_v2/web/app.php/customer/login", myData, request)
  this.http.post("http://dev.ccs.sc/api/security/login", myData, request)
    .subscribe(data => {
      console.log(JSON.stringify(data));
     this.get_response = JSON.stringify(data);
      let a = JSON.parse(JSON.stringify(data));

      console.log(a.isVerified);

      if (a.isVerified =="true"){
        console.log("User is true");
      }
      else
      {
    
      }
     

     }, error => {
      console.log(JSON.stringify(error));
      this.get_response = JSON.stringify(error);
    });
  }
  
  async getresult()
  
  {  

console.log("sending request");


  let loading = await this.loadingCtrl.create();
  await loading.present();


let nativeCall = this.nativeCall.post('http://dev.ccs.sc/api/security/login', this.credentials,{
'Content-Type': 'application/json'
});

from(nativeCall).pipe(
finalize(() => loading.dismiss())
)
.subscribe(data=>{
console.log('native data: ',data);
this.data = JSON.parse(data.data);

}, err => {
console.log('call error',err);
});

 /* let headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST'
  };

  this.nativeCall.post('http://dev.ccs.sc/api/security/login', this.credentials, headers)
              .then(data => {
                  console.log(data);
                  console.log("result"+data);
              })
              .catch(error => {
                  console.log(error);
              });*/
 
    
  /*  let nativeCall = this.nativeCall.get("http://dev.ccs.sc/api/security/login",this.credentials,{
      'Content-Type': 'application/json', 
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST',
      "X-Requested-With": "XMLHttpRequest",
    });
    
    from(this.nativeCall.get("http://dev.ccs.sc/api/security/login", this.credentials, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})).pipe(
      finalize(() => console.log("Result"))
    ).subscribe(
      data => {       console.log(data);
        console.log(data["_body"]); 
              },
     err => { console.log('Native Call error: ', err); }
     );
    */

let yourheaders = new HttpHeaders({
      'Content-Type': 'application/json'
      });
  
      let request = {
      headers: yourheaders
      }
      console.log("http://dev.ccs.sc/api/security/login");
      console.log(JSON.stringify(this.credentials));
      console.log(JSON.stringify(request));
  this.http.post("ttp://dev.ccs.sc/api/security/login", JSON.stringify(this.credentials), request)
    .subscribe(data => {
      console.log(JSON.stringify(data));
      console.log(JSON.stringify(data["_body"]));
      let a = JSON.parse(JSON.stringify(data));
      
    });


  }


  



  onSignup() {
    this.router.navigateByUrl('/signup');
  }
  
  ionViewWillEnter() {
 
    this.menuCtrl.enable(false);
   }
}
