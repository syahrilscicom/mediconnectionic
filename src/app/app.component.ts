import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { NavController} from '@ionic/angular';
import { MenuController, Platform, ToastController, AlertController } from '@ionic/angular';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Storage } from '@ionic/storage';

import { UserData } from './providers/user-data';
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  appPages = [
    {
      title: 'Home',
      url: '/app/tabs/home',
      icon: 'assets/icon/home.svg'
    },
    {
      title: 'Clinic Visits',
      url: '/app/tabs/schedule',
      icon: 'assets/icon/history.svg'
    },
    {
      title: 'Plan & Utilization',
      url: '/app/tabs/myplan',
      icon: 'assets/icon/utilization.svg'
    },
    {
      title: 'Claims  ',
      url: '/app/tabs/claims',
      icon: 'assets/icon/claims.svg'
    },
    {
      title: 'Profile',
      url: '/app/tabs/about',
      icon: 'assets/icon/profile.svg'
    }

    
  
  ];
  loggedIn = false;
  dark = false;

  constructor(
    public alertCtrl: AlertController,
    private menu: MenuController,
    private platform: Platform,
    private router: Router,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private userData: UserData,
    private swUpdate: SwUpdate,
    private toastCtrl: ToastController,
    private nav: NavController,
    private fcm: FCM


  ) {
    this.initializeApp();
   // this.initQrcode();

   this.platform.backButton.subscribeWithPriority(5, () => {
    console.log('Another handler was called!');
  });

  this.platform.backButton.subscribeWithPriority(10, (processNextHandler) => {
    console.log('Handler was called!');

    processNextHandler();
  });

  }

  async ngOnInit() {
   

    this.checkLoginStatus();
    this.listenForLoginEvents();

    this.swUpdate.available.subscribe(async res => {
      const toast = await this.toastCtrl.create({
        message: 'Update available!',
        position: 'bottom',
        buttons: [
          {
            role: 'cancel',
            text: 'Reload'
          }
        ]
      });

      await toast.present();

      toast
        .onDidDismiss()
        .then(() => this.swUpdate.activateUpdate())
        .then(() => window.location.reload());
    });
  }

  initializeApp() {


    this.platform.ready().then(() => {

  

      this.statusBar.styleDefault();
        this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#2f90b0');
      this.splashScreen.hide();


//get fcm token
this.fcm.getToken().then(token => {
  console.log(token);
  // send token to the server
});

//get push noti
this.fcm.onNotification().subscribe(data => {
  console.log(data);
  if (data.wasTapped) {
    console.log('Received in background');
  } else {
    console.log('Received in foreground');
  }
});

    });


    
  }

  ionViewWillLeave(){
    console.log('Leaving App');
  }


  checkLoginStatus() {
    return this.userData.isLoggedIn().then(loggedIn => {
      return this.updateLoggedInStatus(loggedIn);
    });
  }

  updateLoggedInStatus(loggedIn: boolean) {
    setTimeout(() => {
      this.loggedIn = loggedIn;
    }, 300);
  }

  listenForLoginEvents() {
    window.addEventListener('user:login', () => {
      this.updateLoggedInStatus(true);
    });

    window.addEventListener('user:signup', () => {
      this.updateLoggedInStatus(true);
    });

    window.addEventListener('user:logout', () => {
      this.updateLoggedInStatus(false);
    });
  }

  logout() {
  
    this.exit( 'Exit App');
 
  }
  





  async exit( title: string) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: 'Are you sure to exit the App',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
     
          }
        },
        {
          text: 'Yes',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });
    // now present the alert on top of all other content
    await alert.present();
  }

  openTutorial() {
    this.menu.enable(false);
    this.storage.set('ion_did_tutorial', false);
    this.router.navigateByUrl('/tutorial');
  }

}
